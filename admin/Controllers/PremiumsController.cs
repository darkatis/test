﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using admin.Models;
using admin.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace admin.Controllers
{
    public class PremiumsController : Controller
    {
       
        public IActionResult Index()
        {
           
            return View();
        }

        [HttpGet]
        public IActionResult CarModel(String carBrand)
        {
            String DEFAULT_YEAR = "2016";
           
            Call_WebServicescs car = new Call_WebServicescs();
            IRestResponse carmodel = car.CarModels(DEFAULT_YEAR,carBrand);
            return Json(carmodel.Content);    
        }

        [HttpGet]
        public IActionResult CarTrim(String YearManufac)
        {
            
            
            Call_WebServicescs car = new Call_WebServicescs();
            IRestResponse carmodel = car.CarTrims();
            return Json(carmodel.Content);
        }
    }


}