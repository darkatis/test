﻿using System.ComponentModel.DataAnnotations;


namespace admin.Models
{
    public class Customers
    {
        [Key]
        public int id { get; set; }
        public string firsname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string expirydate { get; set; }
        public string carmake { get; set; }
        public string carmodel { get; set; }
        public string cartrim { get; set; }
        public string carregisyear { get; set; }
        public string note { get; set; }
    }
}
