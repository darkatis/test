﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace admin.Services
{
    public class Call_WebServicescs
    {
        public JObject CarMakes()
        {
            var client = new RestClient("https://vgateway.viriyah.co.th/vstorecore/VMICars/CarMakes?VehicleRegisyear=2017");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Basic ZWNvbTM2MDpFQzNvbTYw");
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);

            return JObject.Parse(response.Content);
        }

        public IRestResponse CarModels(String registerYear,String carBrand)
        {
            String requestStr = String.Format("https://vgateway.viriyah.co.th/vstorecore/VMICars/CarModels?VehicleRegisyear={0}&CarMake={1}",registerYear,carBrand);
            var client = new RestClient(requestStr);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Basic ZWNvbTM2MDpFQzNvbTYw");
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);

            return response;
        }

        public IRestResponse CarTrims()
        {
            var client = new RestClient("https://vgateway.viriyah.co.th/vstorecore/VMICars/PriceCarTrims?YearManufac=2017&CarMake=TOYOTA&CarModels=VIOS");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Basic ZWNvbTM2MDpFQzNvbTYw");
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);

            return response;
        }

    }
}
